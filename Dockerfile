FROM openjdk:8-jdk

RUN apt-get update && apt-get install -y --no-install-recommends openjfx && rm -rf /var/lib/apt/lists/*
RUN cp /usr/lib/jvm/java-8-openjdk-amd64/jre/lib/ext/* /usr/local/openjdk-8/jre/lib/ext

# Install gradle
RUN wget https://services.gradle.org/distributions/gradle-5.5.1-bin.zip -P /tmp
RUN unzip -d /opt/gradle /tmp/gradle-*.zip

ENV GRADLE_HOME="/opt/gradle/gradle-5.5.1"
ENV PATH="${GRADLE_HOME}/bin:${PATH}"